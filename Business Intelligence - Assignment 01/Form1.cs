﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraCharts;
using ChartTitle = DevExpress.XtraCharts.ChartTitle;


namespace Business_Intelligence___Assignment_01
{
    public partial class Form1 : Form
    {
        // Generate a data table. 
        DataTable table = new DataTable();
        int intUserData;
        int intNextRow = 0;
        

        public Form1()
        {
            InitializeComponent();
        }

        private DataTable CreateChartData(int rowCount)
        {
            //Create an empty table.
            DataTable table = new DataTable("Table 1");

            //Add two columns to the table. 
            table.Columns.Add("Argument", typeof(Int32));
            table.Columns.Add("Value", typeof(Int32));

            //Add random data rows to the table should the teacher not have the time to generate own data.
            Random rnd = new Random();
            DataRow row = null;
            for(int i = 0; i < rowCount; i++)
            {
                row = table.NewRow();
                row["Argument"] = i;
                row["Value"] = rnd.Next(100);
                table.Rows.Add(row);
            }
            return table;
        }
        

        private void Form1_Load(object sender, EventArgs e)
        {
            //Generate a random table using the CreateChartData method.
            table = CreateChartData(50);

            /*PIE CHART*****************************************************************/
            
            //Generate a Pie Chart, Pie Series with a viewtype of a pie chart
            ChartControl pieChart = new ChartControl();
            Series pieSeries = new Series("Series2", ViewType.Pie);

            //Bind the series to the table
            pieSeries.DataSource = table;

            //Add titles and legend to the pie barChart
            pieChart.Titles.Add(new ChartTitle() { Text = "Pie Chart Caloric Intake" });
            pieChart.Legend.Visibility = DevExpress.Utils.DefaultBoolean.True;

            //Add elements to the pie barChart to change the look
            pieSeries.ArgumentScaleType = ScaleType.Numerical;
            pieSeries.ArgumentDataMember = "Argument";
            pieSeries.ValueScaleType = ScaleType.Numerical;
            pieSeries.ValueDataMembers.AddRange(new string[] { "Value" });

            //Add the pie series to the pie barChart and dock the pie barChart to the data grid view
            pieChart.Series.Add(pieSeries);
            pieChart.Dock = DockStyle.Fill;
            dataGridViewPie.Controls.Add(pieChart);

            /*LINE CHART*****************************************************************/

            //Generate a Line Chart, Line Series with a viewtype of a line chart
            ChartControl lineChart = new ChartControl();
            Series lineSeries = new Series("Caloric Intake", ViewType.Line);

            // Add a title to the chart. 
            lineChart.Titles.Add(new ChartTitle());
            lineChart.Titles[0].Text = "Line Chart Caloric Intake";

            //linking the lines series data source to the generated table
            lineSeries.DataSource = table;

            //Add elements to the pie barChart to change the look
            lineSeries.ArgumentScaleType = ScaleType.Numerical;
            lineSeries.ArgumentDataMember = "Argument";
            lineSeries.ValueScaleType = ScaleType.Numerical;
            lineSeries.ValueDataMembers.AddRange(new string[] { "Value" });

            lineChart.Series.Add(lineSeries);
            lineChart.Dock = DockStyle.Fill;
            dataGridViewLine.Controls.Add(lineChart);

            /*BAR CHART*****************************************************************/

            // Create a barChart. 
            ChartControl barChart = new ChartControl();
            
            // Create an empty Bar series and add it to the barChart and bind the series to it. 
            Series barSeries = new Series("Caloric Intake", ViewType.Bar);
            barChart.Series.Add(barSeries);
            barSeries.DataSource = table;

            // Specify data members to bind the series. 
            barSeries.ArgumentScaleType = ScaleType.Numerical;
            barSeries.ArgumentDataMember = "Argument";
            barSeries.ValueScaleType = ScaleType.Numerical;
            barSeries.ValueDataMembers.AddRange(new string[] { "Value" });

            //Add titles and legend to the bar chart
            barChart.Titles.Add(new ChartTitle() { Text = "Bar Chart Caloric Intake" });
            barChart.Legend.Visibility = DevExpress.Utils.DefaultBoolean.True;

            //Using XYDiagram to add and X title and Y title
            XYDiagram diagram = (XYDiagram)barChart.Diagram;
            diagram.AxisY.Title.Visible = true;
            diagram.AxisX.Title.Visible = true;
            diagram.AxisX.Title.Text = "Days";
            diagram.AxisY.Title.Text = "Caloric Amount";


            // Dock the barChart into its parent and add it to the current form. 
            barChart.Dock = DockStyle.Fill;
            dataGridView1.Controls.Add(barChart);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                intUserData = Int32.Parse(tbDataAmount.Text);

                if (intUserData <= 0)
                {
                    MessageBoxError();
                }
                else
                {
                    //Adding a day for the next value entered and adding the name
                    intNextRow = intNextRow + 1;
                    lbDaysAmount.Text = intNextRow.ToString();
                    lbUsersNameShown.Text = tbUserName.Text;


                    //Generate a row for new information on a day to day bases
                    DataRow newRow = null;
                    newRow = table.NewRow();
                    newRow["Argument"] = intNextRow;
                    newRow["Value"] = intUserData;
                    table.Rows.Add(newRow);
                }
            }
            catch
            {
                MessageBoxError();
            }


        }

        //General user error message added
        private void MessageBoxError()
        {
            MessageBox.Show("Please input text into the Data Name and a value higher " +
                  "than 0 into Data Amount");
            tbUserName.Text = "";
            tbDataAmount.Text = "";
        }

        //Clear data for user to enter new table information
        private void BtnClear_Click(object sender, EventArgs e)
        {
            table.Clear();
            intNextRow = 0;
            lbUsersNameShown.Text = "";
        }
    }
}
