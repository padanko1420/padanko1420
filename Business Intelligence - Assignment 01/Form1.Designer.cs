﻿namespace Business_Intelligence___Assignment_01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nwindDataSet = new Business_Intelligence___Assignment_01.nwindDataSet();
            this.productsTableAdapter = new Business_Intelligence___Assignment_01.nwindDataSetTableAdapters.ProductsTableAdapter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnAddData = new System.Windows.Forms.Button();
            this.dataGridViewPie = new System.Windows.Forms.DataGridView();
            this.tbUserName = new System.Windows.Forms.TextBox();
            this.tbDataAmount = new System.Windows.Forms.TextBox();
            this.lbUserName = new System.Windows.Forms.Label();
            this.lbDataAmount = new System.Windows.Forms.Label();
            this.dataGridViewLine = new System.Windows.Forms.DataGridView();
            this.btnClear = new System.Windows.Forms.Button();
            this.lbFormIdentity = new System.Windows.Forms.Label();
            this.lbUsersNameShown = new System.Windows.Forms.Label();
            this.lbDays = new System.Windows.Forms.Label();
            this.lbDaysAmount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nwindDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLine)).BeginInit();
            this.SuspendLayout();
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "Products";
            this.productsBindingSource.DataSource = this.nwindDataSet;
            // 
            // nwindDataSet
            // 
            this.nwindDataSet.DataSetName = "nwindDataSet";
            this.nwindDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(676, 440);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(658, 481);
            this.dataGridView1.TabIndex = 0;
            // 
            // btnAddData
            // 
            this.btnAddData.Location = new System.Drawing.Point(24, 133);
            this.btnAddData.Name = "btnAddData";
            this.btnAddData.Size = new System.Drawing.Size(75, 23);
            this.btnAddData.TabIndex = 1;
            this.btnAddData.Text = "Add";
            this.btnAddData.UseVisualStyleBackColor = true;
            this.btnAddData.Click += new System.EventHandler(this.Button1_Click);
            // 
            // dataGridViewPie
            // 
            this.dataGridViewPie.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPie.Location = new System.Drawing.Point(676, 13);
            this.dataGridViewPie.Name = "dataGridViewPie";
            this.dataGridViewPie.RowHeadersWidth = 51;
            this.dataGridViewPie.RowTemplate.Height = 24;
            this.dataGridViewPie.Size = new System.Drawing.Size(658, 421);
            this.dataGridViewPie.TabIndex = 2;
            // 
            // tbUserName
            // 
            this.tbUserName.Location = new System.Drawing.Point(12, 36);
            this.tbUserName.Name = "tbUserName";
            this.tbUserName.Size = new System.Drawing.Size(100, 22);
            this.tbUserName.TabIndex = 3;
            // 
            // tbDataAmount
            // 
            this.tbDataAmount.Location = new System.Drawing.Point(12, 92);
            this.tbDataAmount.Name = "tbDataAmount";
            this.tbDataAmount.Size = new System.Drawing.Size(100, 22);
            this.tbDataAmount.TabIndex = 4;
            // 
            // lbUserName
            // 
            this.lbUserName.AutoSize = true;
            this.lbUserName.Location = new System.Drawing.Point(12, 13);
            this.lbUserName.Name = "lbUserName";
            this.lbUserName.Size = new System.Drawing.Size(49, 17);
            this.lbUserName.TabIndex = 5;
            this.lbUserName.Text = "Name:";
            // 
            // lbDataAmount
            // 
            this.lbDataAmount.AutoSize = true;
            this.lbDataAmount.Location = new System.Drawing.Point(12, 65);
            this.lbDataAmount.Name = "lbDataAmount";
            this.lbDataAmount.Size = new System.Drawing.Size(60, 17);
            this.lbDataAmount.TabIndex = 6;
            this.lbDataAmount.Text = "Amount:";
            // 
            // dataGridViewLine
            // 
            this.dataGridViewLine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLine.Location = new System.Drawing.Point(12, 440);
            this.dataGridViewLine.Name = "dataGridViewLine";
            this.dataGridViewLine.RowHeadersWidth = 51;
            this.dataGridViewLine.RowTemplate.Height = 24;
            this.dataGridViewLine.Size = new System.Drawing.Size(642, 481);
            this.dataGridViewLine.TabIndex = 8;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(106, 133);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "Clear Data";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // lbFormIdentity
            // 
            this.lbFormIdentity.AutoSize = true;
            this.lbFormIdentity.Location = new System.Drawing.Point(12, 177);
            this.lbFormIdentity.Name = "lbFormIdentity";
            this.lbFormIdentity.Size = new System.Drawing.Size(434, 68);
            this.lbFormIdentity.TabIndex = 10;
            this.lbFormIdentity.Text = resources.GetString("lbFormIdentity.Text");
            // 
            // lbUsersNameShown
            // 
            this.lbUsersNameShown.AutoSize = true;
            this.lbUsersNameShown.Location = new System.Drawing.Point(118, 39);
            this.lbUsersNameShown.Name = "lbUsersNameShown";
            this.lbUsersNameShown.Size = new System.Drawing.Size(0, 17);
            this.lbUsersNameShown.TabIndex = 11;
            // 
            // lbDays
            // 
            this.lbDays.AutoSize = true;
            this.lbDays.Location = new System.Drawing.Point(118, 95);
            this.lbDays.Name = "lbDays";
            this.lbDays.Size = new System.Drawing.Size(37, 17);
            this.lbDays.TabIndex = 12;
            this.lbDays.Text = "Day:";
            // 
            // lbDaysAmount
            // 
            this.lbDaysAmount.AutoSize = true;
            this.lbDaysAmount.Location = new System.Drawing.Point(168, 95);
            this.lbDaysAmount.Name = "lbDaysAmount";
            this.lbDaysAmount.Size = new System.Drawing.Size(0, 17);
            this.lbDaysAmount.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1375, 1055);
            this.Controls.Add(this.lbDaysAmount);
            this.Controls.Add(this.lbDays);
            this.Controls.Add(this.lbUsersNameShown);
            this.Controls.Add(this.lbFormIdentity);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.dataGridViewLine);
            this.Controls.Add(this.lbDataAmount);
            this.Controls.Add(this.lbUserName);
            this.Controls.Add(this.tbDataAmount);
            this.Controls.Add(this.tbUserName);
            this.Controls.Add(this.dataGridViewPie);
            this.Controls.Add(this.btnAddData);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nwindDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLine)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private nwindDataSet nwindDataSet;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private nwindDataSetTableAdapters.ProductsTableAdapter productsTableAdapter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAddData;
        private System.Windows.Forms.DataGridView dataGridViewPie;
        private System.Windows.Forms.TextBox tbUserName;
        private System.Windows.Forms.TextBox tbDataAmount;
        private System.Windows.Forms.Label lbUserName;
        private System.Windows.Forms.Label lbDataAmount;
        private System.Windows.Forms.DataGridView dataGridViewLine;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lbFormIdentity;
        private System.Windows.Forms.Label lbUsersNameShown;
        private System.Windows.Forms.Label lbDays;
        private System.Windows.Forms.Label lbDaysAmount;
    }
}

